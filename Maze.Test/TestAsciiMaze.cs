﻿
using System;
using Maze.Lib;


namespace Maze.Test
{
	class TestAsciiMaze
	{
		public void Run()
		{
			var src = new StaticAsciiMazeSource();
			Console.WriteLine( "Start Maze:" );
			src.PrintMaze();

			var model = src.GetModel();
			Console.WriteLine( "\r\nModel: " );
			PrintModel( model );
			PrintStartAndEnd( model );

			var algo = new BasicMazeAlgorithm();
			//var path = algo.GetSolutionPath_OLD1( model );
			var path = algo.GetSolutionPath( model );
			src.PlotSolutionPath( path );
			Console.WriteLine( "\r\n\r\nFinal Maze:" );
			src.PrintMaze();
			Console.WriteLine( "Loops to Solution: {0}", algo.LoopCounter );
		}


		void PrintStartAndEnd( IMazeModel model )
		{
			Console.Write( "\r\nStart: {0}, {1} - Neighbors: ", model.StartCell.Y, model.StartCell.X );
			foreach( IMazeCell c in model.GetNeighbors( model.StartCell ) )
				Console.Write( "({0},{1}) ", c.Y, c.X );

			Console.Write( "\r\nEnd: {0}, {1} - Neighbors: ", model.GoalCell.Y, model.GoalCell.X );
			foreach( IMazeCell c in model.GetNeighbors( model.GoalCell ) )
				Console.Write( "({0},{1}) ", c.Y, c.X );
		}


		void PrintModel( IMazeModel model )
		{
			int x, y;
			for( y = 0 ; y < model.Height ; ++y )
			{
				for( x = 0 ; x < model.Width ; ++x )
				{
					var c = model.GetCell( x, y );
					Console.Write( "{0}", ( c.Type.ToString()[ 0 ] ) );
				}
				Console.WriteLine();
			}
		}
	}
}
