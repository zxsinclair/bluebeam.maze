﻿
using Maze.Lib;
using System;


namespace Maze.Test
{
	
	class Program
	{
		static void Main( string[] args )
		{
			//var test1 = new TestAsciiMaze();
			//test1.Run();

			var test2 = new TestBmpMaze();
			test2.Run();

			//Console.WriteLine( "\r\nPress any key..." );
			//Console.ReadKey();
		}

	}
}
