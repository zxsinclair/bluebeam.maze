﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using Maze.Lib;


namespace Maze.Test
{
	class TestBmpMaze
	{
		public void Run()
		{
			var src = new BasicBmpMazeSource();
			TestColors( src );
			TestFileRead( src );
			var model = TestGetModel( src );
			var path = TestGetSolutionPath( model );
			src.PlotSolutionPath( path );
			TestFileWrite( src );
		}


		IEnumerable<IMazeCell> TestGetSolutionPath( IMazeModel model )
		{
			var algo = new BasicMazeAlgorithm();
			var path = algo.GetSolutionPath( model );
			//var path = algo.GetSolutionPath_OLD1( model );
			Console.WriteLine( "Loops to solution: {0}", algo.LoopCounter );
			return path;
		}

		void TestColors( BasicBmpMazeSource src )
		{
			Console.WriteLine( "Start Color: {0}", src.StartColor );
			Console.WriteLine( "Goal Color: {0}", src.GoalColor );
			Console.WriteLine( "Wall Color: {0}", src.WallColor );
			Console.WriteLine( "Solution Color: {0}", src.SolutionColor );
		}


		void TestFileRead( BasicBmpMazeSource src)
		{
			string[] filenames = { "asdf.bmp", "", "maze01.png"};
			foreach( string f in filenames )
				try
				{
					Console.Write( "Reading: {0}... ", f );
					src.ReadFile( f );
					Console.WriteLine( "OK" );
				}
				catch( Exception ex )
				{
					Console.WriteLine( "ERROR: {0}", ex.Message );
				}
		}


		void TestFileWrite( BasicBmpMazeSource src )
		{
			string[] filenames = { "", "maze02.png", "maze02.png" };
			foreach( string f in filenames )
				try
				{
					Console.Write( "Writing: {0}... ", f );
					src.WriteFile( f );
					Console.WriteLine( "OK" );
				}
				catch( Exception ex )
				{
					Console.WriteLine( "ERROR: {0}", ex.Message );
				}
		}


		IMazeModel TestGetModel( BasicBmpMazeSource src )
		{
			var model = src.GetModel();
			Console.WriteLine( "Model Width: {0}, Height: {1}", model.Width, model.Height );

			Console.Write( "Start X: {0}, Y: {1}, Neighbors: ", model.StartCell.X, model.StartCell.Y );
			foreach( IMazeCell c in model.GetNeighbors( model.StartCell ) )
				Console.Write( "({0},{1}) ", c.Y, c.X );

			Console.Write( "\r\nGoal X: {0}, Y: {1}, Neighbors: ", model.GoalCell.X, model.GoalCell.Y );
			foreach( IMazeCell c in model.GetNeighbors( model.GoalCell ) )
				Console.Write( "({0},{1}) ", c.Y, c.X );

			Console.WriteLine();
			return model;
		}
	}
}
