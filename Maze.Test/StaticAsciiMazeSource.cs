﻿
using Maze.Lib;
using System;
using System.Collections.Generic;


namespace Maze.Test
{
	class StaticAsciiMazeSource : IMazeSource
	{
		string[] AsciiMaze =
			//{
			//	"##########",
			//	"#S       #",
			//	"#######  #",
			//	"#        #",
			//	"#  #######",
			//	"#       G#",
			//	"##########"
			//};

			//{
			//	"##################",
			//	"#S         #    G#",
			//	"########   # #####",
			//	"#          #     #",
			//	"#  ############  #",
			//	"#      #         #",
			//	"#      #   #     #",
			//	"#          #     #",
			//	"##################"
			//};

			{
				"##################",
				"#S     #     #  G#",
				"####   #  ####   #",
				"#      #  #      #",
				"#   ####  #  #####",
				"#   #     #  #   #",
				"#      ####  #   #",
				"#                #",
				"##################"
			};

		public IMazeModel GetModel()
		{
			int x, y;
			BasicMazeModel model = new BasicMazeModel( AsciiMaze[0].Length, AsciiMaze.Length );
			
			for( y = 0 ; y < model.Height ; ++y )
				for( x = 0 ; x < model.Width ; ++x )
				{
					var cell = new BasicMazeCell(x, y, ( y * model.Width ) + x);
					switch( AsciiMaze[y][x] )
					{
						case '#': cell.Type = MazeCellType.Wall; break;
						case 'S': cell.Type = MazeCellType.Start; model.StartCell = cell; break;
						case 'G': cell.Type = MazeCellType.Goal; model.GoalCell = cell; break;
						default: cell.Type = MazeCellType.Open; break;
					}
					model.SetCell( cell );
				}
			return model;
		}


		public void PlotSolutionPath( IEnumerable<IMazeCell> cells )
		{
			foreach( IMazeCell c in cells )
			{
				char[] charr = this.AsciiMaze[ c.Y ].ToCharArray();
				charr[ c.X ] = '+';
				this.AsciiMaze[ c.Y ] = new string( charr );
			}
		}


		public void PrintMaze()
		{
			int x, y;
			for( y = 0 ; y < this.AsciiMaze.Length ; ++y )
			{
				for( x = 0 ; x < this.AsciiMaze[ y ].Length ; ++x )
					Console.Write( "{0}", this.AsciiMaze[ y ][ x ] );
				Console.WriteLine();
			}
		}

	}
}
