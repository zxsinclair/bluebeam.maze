﻿
using System;
using Maze.Lib;


namespace MazeApp
{
	
	class Program
	{
		static int Main( string[] args )
		{
			int exitCode = 0;

			if( args.Length != 2 )
			{
				Console.WriteLine( "Usage: maze  maze_file.png  solved_file.png" );
				exitCode = 1;
			}
			else
			{
				string inputFile = args[0];
				string outputFile = args[1];
				try
				{
					var src = new BasicBmpMazeSource();			// Bitmap based source for maze
					src.ReadFile( inputFile );					// Ascii sources are easly implementable (see Maze.Test)
					var model = src.GetModel();					// Sources process raw files/data into models
					var algo = new BasicMazeAlgorithm();		// Algorithms work on models (instead of raw data),
					var path = algo.GetSolutionPath( model );	// can be interchangable, and provide a solution path 
					src.PlotSolutionPath( path );				// that can be plotted back in the source
					src.WriteFile( outputFile );				// Finally, the source writes the output accordingly (bmp, ascii, etc.)
					exitCode = 0;
				}
				catch( Exception ex )
				{
					exitCode = ex.HResult;
					Console.WriteLine( "ERROR: {0}", ex.Message );
				}
			}
			return exitCode;
		}
	}
}
