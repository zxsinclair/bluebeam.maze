﻿
using System;
using System.Collections.Generic;


namespace Maze.Lib
{
	/// <summary>
	/// Maze model interface.  Provides abstraction for mapping different types of maze sources
	/// into a common data structure, which different algorithms can work on.
	/// </summary>
	public interface IMazeModel
	{
		int Width { get; }
		int Height { get; }
		IMazeCell StartCell { get; set; }
		IMazeCell GoalCell { get; set; }
		IMazeCell GetCell( int x, int y );
		IEnumerable<IMazeCell> GetNeighbors( IMazeCell cell );
		void ResetAll();
	}
}
