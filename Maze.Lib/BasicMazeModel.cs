﻿
using System;
using System.Collections.Generic;


namespace Maze.Lib
{
	/// <summary>
	/// Minimalistic implementation of IMazeModel interface
	/// </summary>
	public class BasicMazeModel : IMazeModel
	{
		private IMazeCell[,] _cells = null;

		public int Width { get { return ( _cells != null ? _cells.GetLength( 1 ) : 0 ); } }
		public int Height { get { return ( _cells != null ? _cells.GetLength( 0 ) : 0 ); } }

		public IMazeCell StartCell { get; set; }
		public IMazeCell GoalCell { get; set; }
		
		public BasicMazeModel( int width, int height )
		{
			_cells = new BasicMazeCell[ height, width ];
		}

		public void SetCell( IMazeCell cell )
		{
			_cells[cell.Y, cell.X] = cell;
		}

		public IMazeCell GetCell( int x, int y )
		{
			return _cells[y, x];
		}

		public IEnumerable<IMazeCell> GetNeighbors( IMazeCell cell )
		{
			if( ( cell.X < 0 ) || ( cell.X >= this.Width ) || ( cell.Y < 0 ) || ( cell.Y >= this.Height ) )
				throw new ArgumentOutOfRangeException();

			int x, y, score;
			var list = new List<IMazeCell>();
			for( y = Math.Max( 0, cell.Y - 1 ) ; y < Math.Min( cell.Y + 2, this.Height ) ; ++y )
				for( x = Math.Max( 0, cell.X - 1 ) ; x < Math.Min( cell.X + 2, this.Width ) ; ++x )
					if( ( ( y == cell.Y ) && ( x == cell.X ) ) || ( _cells[ y, x ].Type == MazeCellType.Wall ) || ( _cells[ y, x ].Visited ) )
						continue;
					else
					{ 
						score = _cells[y,x].ScoreFromStart + _cells[y,x].ScoreToGoal;
						list.Add( _cells[ y, x ] );
					}
			return list;
		}

		public void ResetAll()
		{
			int x, y;
			for( y = 0;  y < this.Height;  ++y )
				for( x = 0;  x < this.Width;  ++x )
				{
					_cells[ y, x ].Visited = false;
					_cells[ y, x ].Previous = null;
					_cells[y, x].ScoreFromStart = int.MaxValue;
					_cells[y, x].ScoreToGoal = int.MaxValue;
				}
		}
	}
}
