﻿
using System;

namespace Maze.Lib
{
	/// <summary>
	/// Provides support for mapping raw maze data into models and path finding algorithms
	/// </summary>
	public interface IMazeCell
	{
		int X { get; set; }
		int Y { get; set; }
		MazeCellType Type { get; set; }
		bool Visited { get; set; }
		IMazeCell Previous { get; set; }
		int ScoreFromStart { get; set; }
		int ScoreToGoal { get; set; }
	}
}
