﻿
using System;
using System.Collections.Generic;
using System.Linq;


namespace Maze.Lib
{
	/// <summary>
	/// Implements a basic paht-finding algorithm. It is based on breadth-first search, 
	/// but uses the relative distance from goal to prioritize the search on adjacents cells.
	/// It exposes LoopCounter property for primitive performance analysis.
	/// </summary>
	public class BasicMazeAlgorithm : IMazeAlgorithm
	{
		class CellComparer : IComparer<IMazeCell>
		{
			public int Compare( IMazeCell c1, IMazeCell c2 )
			{
				if( ( c1.X == c2.X ) && ( c1.Y == c2.Y ) ) return 0;

				//int s1 = c1.ScoreFromStart + c1.ScoreToGoal;
				//int s2 = c2.ScoreFromStart + c2.ScoreToGoal;
				int s1 = c1.ScoreToGoal;
				int s2 = c2.ScoreToGoal;
				if( s1 < s2 ) return -1;
				return 1;
			}
		}

		private int PythagorasDistance( IMazeCell c1, IMazeCell c2 )
		{
			return (int) Math.Sqrt( Math.Pow( Math.Abs( c1.X - c2.X ), 2 ) + Math.Pow( Math.Abs( c1.Y - c2.Y ), 2 ) );
		}


		public int LoopCounter { get; private set; }


		// algorithm with distance uses half of iterations, in average, than pure breadth-first
		public IEnumerable<IMazeCell> GetSolutionPath( IMazeModel model )
		{
			model.ResetAll();
			this.LoopCounter = 0;
			var nextVisits = new SortedList<IMazeCell,IMazeCell>( new CellComparer() );
			var solutionList = new List<IMazeCell>();
			var currCell = model.StartCell;
			currCell.Previous = null;
			currCell.Visited = true;
			currCell.ScoreFromStart = PythagorasDistance( currCell, model.StartCell );
			currCell.ScoreToGoal = PythagorasDistance( currCell, model.GoalCell );
			nextVisits.Add( currCell, currCell );
			while( nextVisits.Count > 0 )
			{
				++this.LoopCounter;
				currCell = nextVisits.First().Value;
				nextVisits.Remove( currCell );

				//Console.Write( "Loop: {0}, Curr: ({1},{2}), Set: ", loopCounter, currCell.Y, currCell.X );
				//foreach( IMazeCell c in nextVisits.Values )
				//	Console.Write( "({0},{1}:{2}) ", c.Y, c.X, c.ScoreToGoal );
				//Console.WriteLine();

				if( currCell.Type == MazeCellType.Goal )
				{
					for( ; currCell != null ; currCell = currCell.Previous )
						solutionList.Add( currCell );
					break;
				}

				foreach( IMazeCell visitCell in model.GetNeighbors( currCell ) )
				{
					visitCell.Previous = currCell;
					visitCell.Visited = true;
					visitCell.ScoreFromStart = PythagorasDistance( visitCell, model.StartCell );
					visitCell.ScoreToGoal = PythagorasDistance( visitCell, model.GoalCell );
					nextVisits.Add( visitCell, visitCell );
					//Console.Write( "<{0},{1}> ", visitCell.Y, visitCell.X );
				}
				//Console.WriteLine();
			}
			return solutionList;
		}


		// pure bread-first algorithm
		public IEnumerable<IMazeCell> GetSolutionPath_OLD1( IMazeModel model )
		{
			model.ResetAll();
			this.LoopCounter = 0;
			var nextVisits = new Queue<IMazeCell>();
			var solutionList = new List<IMazeCell>();
			var currCell = model.StartCell;
			currCell.Previous = null;
			nextVisits.Enqueue( currCell );
			currCell.Visited = true;
			while( nextVisits.Count > 0 )
			{
				++this.LoopCounter;
				currCell = nextVisits.Dequeue();

				//Console.Write( "Loop: {0}, Curr: ({1},{2}), Set: ", loopCounter, currCell.Y, currCell.X );
				//foreach( IMazeCell c in nextVisits )
				//	Console.Write( "({0},{1}) ", c.Y, c.X );
				//Console.WriteLine();

				if( currCell.Type == MazeCellType.Goal )
				{
					for( ; currCell != null ; currCell = currCell.Previous )
						solutionList.Add( currCell );
					break;
				}

				foreach( IMazeCell visitCell in model.GetNeighbors( currCell ) )
				{
					visitCell.Previous = currCell;
					visitCell.Visited = true;
					nextVisits.Enqueue( visitCell );
				}
			}
			return solutionList;
		}


	}
}
