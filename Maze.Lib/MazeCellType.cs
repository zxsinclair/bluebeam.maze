﻿
namespace Maze.Lib
{
    public enum MazeCellType
    {
		Open,
		Wall,
		Start,
		Goal
    }
}
