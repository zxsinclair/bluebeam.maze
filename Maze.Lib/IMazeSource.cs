﻿
using System;
using System.Collections.Generic;


namespace Maze.Lib
{
	/// <summary>
	/// Maze source interface.  Abstracts the way mazes are converted into 
	/// generic maze models and the way the solution path can be drawn.
	/// For example, implementations of this interface can translate
	/// bitmap or ascii files into maze models.
	/// </summary>
	public interface IMazeSource
	{
		IMazeModel GetModel();
		void PlotSolutionPath( IEnumerable<IMazeCell> cells );
	}
}
