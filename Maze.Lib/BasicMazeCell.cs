﻿
using System;


namespace Maze.Lib
{
	/// <summary>
	/// Basic implementation of a maze cell.
	/// </summary>
	public class BasicMazeCell : IMazeCell
	{
		public int X { get; set; }
		public int Y { get; set; }
		public MazeCellType Type { get; set; }
		public bool Visited { get; set; }
		public IMazeCell Previous { get; set; }
		public int ScoreFromStart { get; set; }
		public int ScoreToGoal { get; set; }
		public int Id { get; set; }

		public BasicMazeCell()
			: this( 0, 0, 0 )
		{ 
		}

		public BasicMazeCell( int x, int y )
			:this( x, y, 0 )
		{
		}

		public BasicMazeCell( int x, int y, int id )
		{
			this.Id = id;
			this.X = x;
			this.Y = y;
			this.Type = MazeCellType.Open;
			this.Visited = false;
			this.Previous = null;
			this.ScoreFromStart = int.MaxValue;
			this.ScoreToGoal = int.MaxValue;
		}
	}
}
