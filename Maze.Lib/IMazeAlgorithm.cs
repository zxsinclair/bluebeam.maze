﻿
using System;
using System.Collections.Generic;


namespace Maze.Lib
{
	/// <summary>
	/// Interface for maze-solving algorithms.  The single function receives a maze model
	/// and returns a collection of cells that can be plotted back into the maze source.
	/// </summary>
	public interface IMazeAlgorithm
	{
		IEnumerable<IMazeCell> GetSolutionPath( IMazeModel model );
	}
}
