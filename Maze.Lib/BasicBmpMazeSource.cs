﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;


namespace Maze.Lib
{
	/// <summary>
	/// Basic implementation of a bitmap based source.
	/// Provides methods to read and write bitmap files and
	/// can be customized to use different colors for 
	/// Start, Goal, Walls, and Solution Path
	/// </summary>
	public class BasicBmpMazeSource : IMazeSource
	{
		private Bitmap _bmp = null;

		public Color StartColor { get; set; }
		public Color GoalColor { get; set; }
		public Color WallColor { get; set; }
		public Color SolutionColor { get; set; }


		public BasicBmpMazeSource()
		{
			this.StartColor = Color.Blue;
			this.GoalColor = Color.Red;
			this.WallColor = Color.Black;
			this.SolutionColor = Color.Green;
		}

		public BasicBmpMazeSource( Color startColor, Color goalColor, Color wallColor, Color solutionColor )
		{
			this.StartColor = startColor;
			this.GoalColor = goalColor;
			this.WallColor = wallColor;
			this.SolutionColor = solutionColor;
		}


		public void ReadFile( string bmpFile )
		{
			if( string.IsNullOrWhiteSpace( bmpFile ) ) throw new ArgumentException( "Invalid file name" );
			if( !File.Exists( bmpFile ) ) throw new FileNotFoundException();
			_bmp = new Bitmap( bmpFile );
		}


		public void WriteFile( string bmpFile )
		{
			_bmp.Save( bmpFile );
		}

		
		public IMazeModel GetModel()
		{
			if( _bmp == null ) throw new InvalidOperationException( "Image not loaded" );

			int x, y;
			var wall = this.WallColor.ToArgb();
			var start = this.StartColor.ToArgb();
			var goal = this.GoalColor.ToArgb();
			var model = new BasicMazeModel( _bmp.Width, _bmp.Height );

			for( y = 0 ; y < model.Height ; ++y )
				for( x = 0 ; x < model.Width ; ++x )
				{
					int pixel = _bmp.GetPixel(x, y).ToArgb();
					var cell = new BasicMazeCell( x, y, ( y * model.Width ) + x );
					if( pixel == wall )
					{
						cell.Type = MazeCellType.Wall;
					}
					else if( pixel == start )
					{
						cell.Type = MazeCellType.Start;
						if( model.StartCell == null ) model.StartCell = cell;
					}
					else if( pixel == goal )
					{
						cell.Type = MazeCellType.Goal;
						if( model.GoalCell == null ) model.GoalCell = cell;
					}
					else
					{
						cell.Type = MazeCellType.Open;
					}
					model.SetCell( cell );
				}
			return model;
		}


		public void PlotSolutionPath( IEnumerable<IMazeCell> path )
		{
			if( _bmp == null ) throw new InvalidOperationException( "Image not loaded" );

			foreach( IMazeCell cell in path )
				_bmp.SetPixel( cell.X, cell.Y, this.SolutionColor );
		}
	}
}
