# README #

Maze solving program.  Takes a bitmap image as input and outputs another image file with a solution path.  Colors indicate rules for the maze: blue is start, red is goal, black is wall, other colors are open space.

Usage: maze <original_maze>.png <solved_maze>.png

Implementation details:

- Maze Source: handles raw maze file (bmp, png, ascii, etc.). Generate maze models and handle writing solution path back to original file.
- Maze Model and Cells: Abstract maze to a generic form, so algorithms don't need to know details about original maze source.  Also provide support for search algorithms, handling different cell states such as visited and neighbor cells.
- Maze Algorithm: work on maze models and provide solving logic.

This design is intended so parts can be interchangeable. Same algorithm can solve a maze from a .png or ascii file, by just replacing the maze source class. Or adding different solving algorithms for the same source, to compare efficiency.


